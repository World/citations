# Portuguese translation for citations.
# Copyright (C) 2022 citations's COPYRIGHT HOLDER
# This file is distributed under the same license as the citations package.
# Hugo Carvalho <hugokarvalho@hotmail.com>, 2022, 2023, 2024.
#
msgid ""
msgstr ""
"Project-Id-Version: citations master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/World/citations/-/issues\n"
"POT-Creation-Date: 2024-03-05 20:36+0000\n"
"PO-Revision-Date: 2024-05-18 11:42+0100\n"
"Last-Translator: Hugo Carvalho <hugokarvalho@hotmail.com>\n"
"Language-Team: Portuguese <pt@li.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.4.4\n"

#: cratebibtex/src/entry_type.rs:117
msgid "Article"
msgstr "Artigo"

#: cratebibtex/src/entry_type.rs:118
msgid "Book"
msgstr "Livro"

#: cratebibtex/src/entry_type.rs:119
msgid "Misc"
msgstr "Diversos"

#. NOTE As in conference proceedings, see https://en.wikipedia.org/wiki/Conference_proceeding
#: cratebibtex/src/entry_type.rs:121
msgid "In Proceedings"
msgstr "Em Atas"

#: cratebibtex/src/entry_type.rs:122
msgid "Unpublished"
msgstr "Não publicado"

#. NOTE As in online source
#: cratebibtex/src/entry_type.rs:124
msgid "Online"
msgstr "Online"

#. NOTE As in other kind of source
#: cratebibtex/src/entry_type.rs:126
msgid "Other"
msgstr "Outros"

#: cratebibtex/src/entry_type.rs:127
msgid "Booklet"
msgstr "Folheto"

#: cratebibtex/src/entry_type.rs:128
msgid "Conference"
msgstr "Conferência"

#: cratebibtex/src/entry_type.rs:129
msgid "In Book"
msgstr "Em livro"

#: cratebibtex/src/entry_type.rs:130
msgid "In Collection"
msgstr "Em coleção"

#. NOTE As in instruction manual
#: cratebibtex/src/entry_type.rs:132
msgid "Manual"
msgstr "Manual"

#: cratebibtex/src/entry_type.rs:133
msgid "Master's Thesis"
msgstr "Tese de mestrado"

#: cratebibtex/src/entry_type.rs:134
msgid "PhD Thesis"
msgstr "Tese de doutoramento"

#. NOTE As in conference proceedings, see https://en.wikipedia.org/wiki/Conference_proceeding
#: cratebibtex/src/entry_type.rs:136
msgid "Proceedings"
msgstr "Atas"

#: cratebibtex/src/entry_type.rs:137
msgid "Tech Report"
msgstr "Relatório tecnológico"

#. NOTE Plain as in Plain Text
#: cratebibtex/src/format.rs:52
msgid "Plain"
msgstr "Simples"

#. NOTE: This is the app name
#: data/org.gnome.World.Citations.desktop.in.in:3
#: data/org.gnome.World.Citations.metainfo.xml.in.in:7
#: data/resources/ui/window.ui:69 src/main.rs:42 src/window.rs:620
msgid "Citations"
msgstr "Citações"

#: data/org.gnome.World.Citations.desktop.in.in:4
#: data/org.gnome.World.Citations.metainfo.xml.in.in:8 src/window.rs:622
msgid "Manage your bibliography"
msgstr "Gerir a sua bibliografia"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.World.Citations.desktop.in.in:10
msgid "bibliography;citing;literature;BibTeX;LaTeX;TeX;"
msgstr "bibliografia;citando;literatura;BibTeX;LaTex;TeX;"

#: data/org.gnome.World.Citations.desktop.in.in:19
msgid "New Window"
msgstr "Nova janela"

#: data/org.gnome.World.Citations.gschema.xml.in:6
msgid "Window width"
msgstr "Largura da janela"

#: data/org.gnome.World.Citations.gschema.xml.in:10
msgid "Window height"
msgstr "Altura da janela"

#: data/org.gnome.World.Citations.gschema.xml.in:14
msgid "Window maximized state"
msgstr "Estado de janela maximizada"

#: data/org.gnome.World.Citations.gschema.xml.in:18
msgid "List of paths of recent files"
msgstr "Lista de caminhos de ficheiros recentes"

#: data/org.gnome.World.Citations.gschema.xml.in:22
#: data/org.gnome.World.Citations.gschema.xml.in:26
msgid "Default template for book citation"
msgstr "Modelo padrão para citação de livro"

#: data/org.gnome.World.Citations.gschema.xml.in:30
msgid "Default template kind for citations"
msgstr "Tipo de modelo padrão para citações"

#: data/org.gnome.World.Citations.gschema.xml.in:34
msgid "Default template format for citations"
msgstr "Formato de modelo padrão para citações"

#: data/org.gnome.World.Citations.metainfo.xml.in.in:10
msgid "Manage your bibliographies using the BibTeX format."
msgstr "Gerir as suas bibliografias usando o formato BibTeX."

#: data/org.gnome.World.Citations.metainfo.xml.in.in:15
msgid "Main window"
msgstr "Janela principal"

#. Translators: Selects a PDF file for the citation
#: data/resources/ui/add_pdf_dialog.ui:8 data/resources/ui/entry_page.ui:268
msgid "Add PDF"
msgstr "Adicionar PDF"

#: data/resources/ui/add_pdf_dialog.ui:46
msgid "Bibliography folder not set"
msgstr "Pasta de bibliografia não definida"

#: data/resources/ui/add_pdf_dialog.ui:64
msgid ""
"Creates a copy of the selected pdf and stores it in your Bibliography folder."
msgstr ""
"Cria uma cópia do pdf selecionado e armazena-o na sua pasta Bibliografia."

#: data/resources/ui/add_pdf_dialog.ui:79
msgid "From File"
msgstr "Do ficheiro"

#: data/resources/ui/add_pdf_dialog.ui:92
msgid "From URL"
msgstr "Do URL"

#: data/resources/ui/add_pdf_dialog.ui:98
msgid "Download…"
msgstr "Transferir…"

#: data/resources/ui/citation_preview.ui:15
msgid "Custom Citation Format"
msgstr "Formato de citação personalizado"

#: data/resources/ui/citation_preview.ui:50
msgid "Citation"
msgstr "Citação"

#: data/resources/ui/citation_preview.ui:94 data/resources/ui/entry_page.ui:113
msgid "Copy"
msgstr "Copiar"

#: data/resources/ui/citation_preview.ui:137
msgid "Custom Template"
msgstr "Modelo personalizado"

#: data/resources/ui/citation_preview.ui:153
msgid ""
"Write a custom format for your citations, to insert the contents of a tag "
"<tt>sometag</tt> write <tt>%SOMETAG%</tt>. Basic markdown syntax for bold "
"and italics is supported.\n"
"            "
msgstr ""
"Escreva um formato personalizado para as suas citações, para inserir o "
"conteúdo de uma etiqueta <tt>algumaetiqueta</tt> escreva <tt>%SOMETAG%</tt>. "
"É suportada uma sintaxe básica de marcação para negrito e itálico.\n"
"            "

#: data/resources/ui/citation_preview.ui:159
msgid "Example:"
msgstr "Exemplo:"

#. TRANSLATORS Empty state widget
#: data/resources/ui/entry_list.ui:9
msgid "No Citations"
msgstr "Sem citações"

#: data/resources/ui/entry_list.ui:10
msgid "Create a new citation"
msgstr "Criar uma nova citação"

#: data/resources/ui/entry_list_row.ui:90
msgid "_Open PDF"
msgstr "A_brir PDF"

#: data/resources/ui/entry_list_row.ui:94
msgid "_Copy Key"
msgstr "C_opiar chave"

#: data/resources/ui/entry_list_row.ui:100 data/resources/ui/window.ui:61
msgid "_Delete"
msgstr "_Eliminar"

#: data/resources/ui/entry_page.ui:13
msgid "No Citation Selected"
msgstr "Nenhuma citação selecionada"

#: data/resources/ui/entry_page.ui:82
msgid "Citation _Type"
msgstr "_Tipo de citação"

#: data/resources/ui/entry_page.ui:98
msgid "_Other Citation Type"
msgstr "_Outro tipo de citação"

#: data/resources/ui/entry_page.ui:105
msgid "_Citation Key"
msgstr "_Chave de citação"

#: data/resources/ui/entry_page.ui:124
msgid "_Author"
msgstr "_Autor"

#: data/resources/ui/entry_page.ui:130
msgid "_Title"
msgstr "_Título"

#: data/resources/ui/entry_page.ui:136
msgid "_Year"
msgstr "Ano"

#: data/resources/ui/entry_page.ui:152 src/entry_page.rs:403
msgid "Open in Browser"
msgstr "Abrir no navegador"

#: data/resources/ui/entry_page.ui:168
msgid "Other Properties"
msgstr "Outras propriedades"

#: data/resources/ui/entry_page.ui:182
msgid "Abstract"
msgstr "Abstrato"

#: data/resources/ui/entry_page.ui:186
msgid "Copy Abstract"
msgstr "Copiar abstrato"

#: data/resources/ui/entry_page.ui:218
msgid "Notes"
msgstr "Notas"

#: data/resources/ui/entry_page.ui:222
msgid "Copy Notes"
msgstr "Copiar notas"

#: data/resources/ui/entry_page.ui:286
msgid "PDF"
msgstr "PDF"

#: data/resources/ui/entry_page.ui:297 data/resources/ui/window.ui:10
#: data/resources/ui/window.ui:107 data/resources/ui/window.ui:157
msgid "_Open"
msgstr "Abrir"

#: data/resources/ui/entry_page.ui:310
msgid "Delete PDF"
msgstr "Eliminar PDF"

#: data/resources/ui/entry_page.ui:351
msgid "Could not set PDF Preview"
msgstr "Não foi possível definir a pré-visualização PDF"

#: data/resources/ui/new_entry_dialog.ui:7
msgid "Add new Entry"
msgstr "Adicionar nova entrada"

#: data/resources/ui/new_entry_dialog.ui:17
msgid "_Cancel"
msgstr "_Cancelar"

#: data/resources/ui/new_entry_dialog.ui:24
msgid "C_reate"
msgstr "C_riar"

#: data/resources/ui/new_entry_dialog.ui:40
msgid "New"
msgstr "Novo"

#: data/resources/ui/new_entry_dialog.ui:59
msgid "Citation Key"
msgstr "Chave de citação"

#: data/resources/ui/new_entry_dialog.ui:66
msgid "Citation Type"
msgstr "Tipo de citação"

#. TRANSLATORS BibTeX is a proper name
#: data/resources/ui/new_entry_dialog.ui:86
msgid "From BibTeX"
msgstr "De BibTex"

#: data/resources/ui/new_entry_dialog.ui:105
msgid "From DOI"
msgstr "De DOI"

#. TRANSLATORS DOI is a proper name
#: data/resources/ui/new_entry_dialog.ui:116
msgid "DOI"
msgstr "DOI"

#: data/resources/ui/shortcuts.ui:11
msgctxt "shortcut window"
msgid "General"
msgstr "Geral"

#: data/resources/ui/shortcuts.ui:14
msgctxt "shortcut window"
msgid "Show Shortcuts"
msgstr "Mostrar atalhos"

#: data/resources/ui/shortcuts.ui:20
msgctxt "shortcut window"
msgid "New Window"
msgstr "Nova janela"

#: data/resources/ui/shortcuts.ui:26
msgctxt "shortcut window"
msgid "Quit"
msgstr "Sair"

#: data/resources/ui/shortcuts.ui:34
msgctxt "shortcut window"
msgid "Bibliography"
msgstr "Bibliografia"

#: data/resources/ui/shortcuts.ui:37
msgctxt "shortcut window"
msgid "Open"
msgstr "Abrir"

#: data/resources/ui/shortcuts.ui:43
msgctxt "shortcut window"
msgid "Save"
msgstr "Guardar"

#: data/resources/ui/shortcuts.ui:49
msgctxt "shortcut window"
msgid "Search"
msgstr "Pesquisar"

#: data/resources/ui/window.ui:6
msgid "_Create New Bibliography"
msgstr "_Criar nova bibliografia"

#: data/resources/ui/window.ui:17 data/resources/ui/window.ui:39
msgid "_Keyboard Shortcuts"
msgstr "_Teclas de atalho"

#. TRANSLATORS About dialog
#: data/resources/ui/window.ui:21 data/resources/ui/window.ui:43
msgid "_About Citations"
msgstr "_Acerca do Citações"

#: data/resources/ui/window.ui:29
msgid "_Save"
msgstr "_Guardar"

#: data/resources/ui/window.ui:33
msgid "New _Window"
msgstr "Nova _janela"

#: data/resources/ui/window.ui:51
msgid "_Search in Google Scholar"
msgstr "_Procurar no Google Scholar"

#. TRANSLATORS arXiv is a proper name
#: data/resources/ui/window.ui:55
msgid "_Search in arXiv"
msgstr "_Procurar no arXiv"

#: data/resources/ui/window.ui:90 data/resources/ui/window.ui:130
#: data/resources/ui/window.ui:210
msgid "Main Menu"
msgstr "Menu principal"

#. TRANSLATORS BibTeX is a proper name
#: data/resources/ui/window.ui:100
msgid "Open a BibTeX File"
msgstr "Abrir um ficheiro BibTeX"

#: data/resources/ui/window.ui:101
msgid "Or drag-and-drop a file"
msgstr "Ou arrastar e largar um ficheiro"

#: data/resources/ui/window.ui:139
msgid "Manage your Citations"
msgstr "Gerir as suas citações"

#: data/resources/ui/window.ui:149
msgid "Recent Files"
msgstr "Ficheiros recentes"

#: data/resources/ui/window.ui:192
msgid "Sidebar"
msgstr "Barra lateral"

#: data/resources/ui/window.ui:203
msgid "Add New Citation"
msgstr "Adicionar nova citação"

#: data/resources/ui/window.ui:217
msgid "Search"
msgstr "Pesquisar"

#: data/resources/ui/window.ui:231
msgid "Search…"
msgstr "Pesquisar…"

#: data/resources/ui/window.ui:251
msgid "Content"
msgstr "Conteúdo"

#: data/resources/ui/window.ui:261
msgid "Secondary Menu"
msgstr "Menu secundário"

#: src/add_pdf_dialog.rs:58
msgid "Could not set PDF"
msgstr "Não foi possível definir PDF"

#: src/add_pdf_dialog.rs:78
msgid "Could not download PDF"
msgstr "Não foi possível transferir PDF"

#. TRANSLATORS Do NOT translate {path}, it is a variable name
#: src/add_pdf_dialog.rs:104
msgid "Make sure that {path} exists"
msgstr "Certifique-se de que {path} existe"

#: src/add_pdf_dialog.rs:185
msgid "PDF Files"
msgstr "Ficheiros PDF"

#: src/add_pdf_dialog.rs:193
msgid "Select a PDF"
msgstr "Selecione um PDF"

#. TODO Should this be translatable?
#: src/citation_preview.rs:56
msgid "Chicago"
msgstr "Chicago"

#: src/citation_preview.rs:57
msgid "Harvard"
msgstr "Harvard"

#: src/citation_preview.rs:58
msgid "Vancouver"
msgstr "Vancouver"

#. NOTE: Custom as in Custom Template.
#: src/citation_preview.rs:60
msgid "Custom"
msgstr "Personalizado"

#: src/citation_preview.rs:267 src/entry_list_row.rs:59 src/entry_page.rs:243
msgid "Copied"
msgstr "Copiado"

#: src/citation_preview.rs:274
msgid "Custom…"
msgstr "Personalizado…"

#: src/entry_page.rs:204
msgid "Other…"
msgstr "Outros…"

#. TRANSLATORS Every citation has an identifier called key
#: src/entry_page.rs:266
msgid "Citation Key already exists"
msgstr "A chave de citação já existe"

#: src/entry_page.rs:586
msgid "PDF added"
msgstr "PDF adicionado"

#: src/new_entry_dialog.rs:149
msgid "Insert a BibTeX citation here"
msgstr "Insira uma citação BibTeX aqui"

#: src/utils.rs:14
msgid "Author"
msgstr "Autor"

#: src/utils.rs:15
msgid "Year"
msgstr "Ano"

#: src/utils.rs:16
msgid "Title"
msgstr "Título"

#: src/utils.rs:17
msgid "Volume"
msgstr "Volume"

#: src/utils.rs:18
msgid "Number"
msgstr "Número"

#: src/utils.rs:19
msgid "Pages"
msgstr "Páginas"

#: src/utils.rs:20
msgid "Publisher"
msgstr "Editor"

#: src/utils.rs:21
msgid "Journal"
msgstr "Jornal"

#: src/utils.rs:22
msgid "Address"
msgstr "Endereço"

#. TRANSLATORS Method of publication, see https://www.bibtex.com/f/howpublished-field/
#: src/utils.rs:24
msgid "How Published"
msgstr "Como publicado"

#: src/utils.rs:25
msgid "Note"
msgstr "Nota"

#: src/utils.rs:26
msgid "Book Title"
msgstr "Título do Livro"

#: src/utils.rs:27
msgid "Series"
msgstr "Séries"

#: src/utils.rs:28
msgid "Archive Prefix"
msgstr "Prefixo de arquivo"

#. TRANSLATORS As in digital print
#: src/utils.rs:30
msgid "ePrint"
msgstr "ePrint"

#. TRANSLATORS Identifier used by arXiv, see https://arxiv.org/help/hypertex/bibstyles
#: src/utils.rs:32
msgid "Primary Class"
msgstr "Classe primária"

#: src/utils.rs:33
msgid "Month"
msgstr "Mês"

#. TRANSLATORS As in chief editor
#: src/utils.rs:35
msgid "Editor"
msgstr "Editor"

#. TRANSLATORS As in association
#: src/utils.rs:37
msgid "Organization"
msgstr "Organização"

#: src/utils.rs:38
msgid "School"
msgstr "Escola"

#: src/utils.rs:39
msgid "Institution"
msgstr "Instituição"

#: src/window.rs:117
msgid "Could not add entry"
msgstr "Não foi possível adicionar entrada"

#: src/window.rs:331 src/window.rs:356
msgid "BibTeX Files"
msgstr "Ficheiros BibTeX"

#: src/window.rs:365
msgid "Create New Bibliography"
msgstr "Criar nova bibliografia"

#: src/window.rs:366
msgid "_Create"
msgstr "_Criar"

#. TRANSLATORS This a file name, .bib will be appended e.g.
#. bibliography.bib, please only use alphabetic characters.
#: src/window.rs:369
msgid "bibliography"
msgstr "bibliografia"

#: src/window.rs:473
msgid "Remove From Recent Files"
msgstr "Remover de ficheiros recentes"

#: src/window.rs:513
msgid "An entry with the given citation key already exists"
msgstr "Já existe uma entrada com a chave de citação dada"

#: src/window.rs:528
msgid "Bibliography saved"
msgstr "Bibliografia guardada"

#: src/window.rs:581
msgid "Citation Deleted"
msgstr "Citação eliminada"

#: src/window.rs:583
msgid "Undo"
msgstr "Anular"

#: src/window.rs:626
msgid "translator-credits"
msgstr "Hugo Carvalho <hugokarvalho@hotmail.com>"

#. NOTE Dialog title which informs the user about unsaved changes.
#: src/window.rs:638
msgid "Unsaved Changes"
msgstr "Alterações não guardadas"

#. NOTE Dialog subtitle which informs the user about unsaved changes more detailed.
#: src/window.rs:641
msgid "Do you want to write all changes to the safe?"
msgstr "Quer guardar todas as alterações no cofre?"

#: src/window.rs:645
msgid "_Quit Without Saving"
msgstr "_Sair sem guardar"

#: src/window.rs:646
msgid "_Don't Quit"
msgstr "_Não sair"

#: src/window.rs:647
msgid "_Save and Quit"
msgstr "_Guardar e sair"

#~ msgid "Maximiliano Sandoval"
#~ msgstr "Maximiliano Sandoval"

#~ msgid "Back"
#~ msgstr "Voltar"
